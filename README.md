# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Draw straight line                                 | 1~5%     | Y         |


---

### How to use 

>![](https://i.imgur.com/HeIZjD4.jpg)　![](https://i.imgur.com/bJqZAYg.jpg)　![](https://i.imgur.com/hruV0ic.jpg)　![](https://i.imgur.com/x5mpBja.jpg)　![](https://i.imgur.com/Ir2rQ6f.jpg)　![](https://i.imgur.com/igcTwT8.jpg)
>　　畫筆 　 　　橡皮擦　　　輸入文字　　　畫圓形 　　　畫三角形 　　　畫矩形 

>![](https://i.imgur.com/9Mv5f48.jpg)　![](https://i.imgur.com/fBB20BI.jpg)　![](https://i.imgur.com/5kT74VJ.jpg)　![](https://i.imgur.com/QBtex7f.jpg)　![](https://i.imgur.com/Wk3xqy0.jpg)　![](https://i.imgur.com/wBrhyo9.jpg)
>　畫直線　　　　下載　　　　　上傳　　　　Undo　　　　Redo 　　　　Reflesh

> ![](https://i.imgur.com/GrNjo0Q.jpg)
>　調整筆刷大小

>![](https://i.imgur.com/sjaNo5L.jpg)
>　調整字體大小

>![](https://i.imgur.com/5A1vRrl.jpg)
>　調整字型

>![](https://i.imgur.com/ngm8dgG.jpg)　
>　調整筆刷和字體顏色

* 畫筆和畫圖形皆是點完按鈕後到畫布上使用即可，作畫過程中若鼠標超出畫布，則會暫時停止其功能
* 輸入文字是用方法是點完按鈕後，點一下畫布出現文字框，輸入完文字後，用滑鼠點一下畫布即可將文字畫上去
* 其他功能都是按一下按鈕直接可以使用，其中如果在Undo的過後，有使用畫筆或是圖形改變畫布內容，是無法用Redo回到之前的畫面

### Function description
>function補充說明:
>1. initApp( ) : 負責監聽所有的按鈕和畫布事件，隨後進到相對的function
>2. init{功能}( ) : 各個功能前的前置設定
>3. mouseDown( )/mouseMove( )/mouseUp( ) : 負責判斷筆刷在canvas1上的動作和位置、呼叫setBrushColor( )設定筆刷顏色，並視功能設定畫布合成效果和隨後進入相對應的function
>4. mouse2Move( )/mouse2Up( ) : 在畫圖形時會用到，負責判斷筆刷在canvas2上的動作和位置，並視圖形形狀進入相對應的function
>5. setBrushSize( ):設定筆刷大小
>6. setBrushColor( ):設定筆刷字體顏色
>7. setBrushCap( ) : 設定筆頭形狀
>8. upFontSize( )/downFontSize( ):改變字體大小的值
>9. upFontType( )/downFontType( ): 改變字型的值
>10. setFont( ) : 同時設定字體大小和字型 
>11. save( ) : 將現在畫布上的圖片存到history[ ]中

>畫筆
> initApp( ) -> initPencil( ) -> mouseDown( ) -> mouseMove( ) -> draw( ) -> mouseUp( ) -> draw( )
>* 合成效果是使用source-over，筆頭是使用round比較圓滑，draw( )畫到畫布上

>橡皮擦
>initApp( ) ->  initEraser() -> mouseDown( ) -> mouseMove( ) -> erase( ) -> mouseUp( ) -> erase( )
>* 合成效果是使用destination-out，筆頭是使用square比較符合我的橡皮擦形狀，erase( )擦掉經過的路線

>輸入文字
>initApp( ) ->  initText( ) -> mouseDown( ) -> mouseUp( ) -> inputText( ) ->  mouseDown( ) -> finishInput( ) -> drawText( )
>* inputText( )中創建文字框，finishInput( )中刪除文字框，drawText( )將文字畫到畫布上

>畫圖形
>initApp( ) ->  init{圖形}() -> mouseDown( ) -> mouseMove2( ) -> draw{圖形}Canvas2( ) -> mouseUp2() -> draw{圖形}Canvas1( )
>* 在圖形確定前會在第二張畫布上畫，畫布2的合成效果是使用destination-atop達到縮放大小的效果，確定後再將圖形畫到畫布1上，並清空畫布2

>下載
>initApp( ) -> initDownload( ) ->  Download( ) 
>* 在 Download( ) 中呼叫canvas.toDataURL( )下載圖片

>上傳
>initApp( ) ->  initUpload()  -> loadFile( ) 
>* 我將<input>隱藏起來，藉由button來呼叫他的.click()，進入loadFile( )上傳圖片

>Undo
>initApp( ) -> initUndo( ) ->  Undo( ) 
>* history[ ]中有存每一步的圖片，運用step跟maxStep來判斷去history[ ]中的哪個index抓圖片

>Redo
>initApp( ) -> initRedo( ) -> Redo( )
>* history[ ]中有存每一步的圖片，運用step跟maxStep來判斷去history[ ]中的哪個index抓圖片

>Reset
>initApp( ) -> initReflesh( ) -> Reflesh( )
>* 在Reflesh( )中將畫布清空，運用context.clearRect( )

>筆刷大小
>initApp( ) -> downBrushSize( )/upBrushSize( ) -> setBrushSize( ) 
>* 在downBrushSize( )/upBrushSize( )改變顯示的數字並取得其值，在setBrushSize( )中設定筆刷大小

>字體大小
>initApp( ) -> downFontSize( )/upFontSize( ) -> setFont( )
>* 在downFontSize( )/upFontSize( )改變顯示的數字並取得其值，在setFont( )中設定字體大小和字型

>字型
>initApp( ) -> downFontType( )/upFontType( ) -> setFont( )
>* 在downFontType( )/upFontType( )改變顯示的文字並取得其值，在setFont( )中設定字體大小和字型

>顏色
>setBrushColor( ) 
>* 直接取得input.value改顏色




### Gitlab page link

https://105072132.gitlab.io/AS_01_WebCanvas/

### Others (Optional)



<style>
table th{
    width: 100%;
}
</style>