function mouseDown(e) {
    console.log("mouseDown");
    judgeLeave1 = 0;
    setBrushColor();
    x = e.clientX - paper.left;
    y = e.clientY - paper.top;
    shapeX = x + 8;
    shapeY = y;
    if (pencil === true){
        context.globalCompositeOperation = 'source-over';
        isDrawing = true;
    }
    if (eraser === true){
        context.globalCompositeOperation = 'destination-out';
        isErasing = true;
    }
    if (isTexting === true){
        context.globalCompositeOperation = 'source-over';
        finishInput();
        if(afterundo){
            maxStep = step;
            afterundo = false;
        }
        save();
        isTexting = false;
    }else if(isTexting === false && text === true){
        isTexting = true;
    }
    if (circle === true){
        canvas2.style.zIndex = 1;
        canvas.style.zIndex= 0;
        isCircle  = true;
    }
    if (triangle === true){
        canvas2.style.zIndex = 1;
        canvas.style.zIndex= 0;
        isTriangle  = true;
    }
    if (rectangle === true){
        canvas2.style.zIndex = 1;
        canvas.style.zIndex= 0;
        isRectangle  = true;
    }
    if (line === true){
        canvas2.style.zIndex = 1;
        canvas.style.zIndex= 0;
        isLine  = true;
    }
}

function mouseMove(e) {
    console.log("mouseMove");
    if (isDrawing === true) {
        draw(x, y, e.clientX - paper.left, e.clientY - paper.top);
    }
    if (isErasing === true) {
        context.globalCompositeOperation = 'destination-out';
        erase(x, y, e.clientX - paper.left, e.clientY - paper.top);
    }
    x = e.clientX - paper.left;
    y = e.clientY - paper.top;
}

function mouseUp(e) {
    console.log("mouseUp");
    if (isDrawing === true) {
        draw(x, y, e.clientX - paper.left, e.clientY - paper.top);
        isDrawing = false;
        if(afterundo){
            maxStep = step;
            afterundo = false;
        }
        save();
    }
    if (isErasing === true) {
        context.globalCompositeOperation = 'destination-out';
        erase(x, y, e.clientX - paper.left, e.clientY - paper.top);
        isErasing = false;
        if(afterundo){
            maxStep = step;
            afterundo = false;
        }
        save();
    }
    if (isTexting === true && judgeLeave1 == 0){
        inputText(x, y);
        isTexting = true;
    }
    x = 0;
    y = 0;
}

function mouseMove2(e){
    console.log("mouseMove2");
    if (isCircle === true) {
        drawCircleCanvas2(e.clientX - paper.left + 8, e.clientY - paper.top, shapeX, shapeY);
    }
    if (isTriangle === true){
        drawTriangleCanvas2(e.clientX - paper.left + 8, e.clientY - paper.top, shapeX, shapeY);
    }
    if (isRectangle === true) {
        drawRectangleCanvas2(e.clientX - paper.left, e.clientY - paper.top, shapeX - 8, shapeY);
    }
    if (isLine === true) {
        drawLineCanvas2(e.clientX - paper.left, e.clientY - paper.top, shapeX - 8, shapeY);
    }
}

function mouseUp2(e){
    console.log("mouseUp2");
    context.globalCompositeOperation = 'source-over';
    if (isCircle === true) {
        drawCircleCanvas1(e.clientX - paper.left + 8, e.clientY - paper.top, shapeX, shapeY);
        context2.clearRect(0, 0, canvas2.width, canvas2.height);
        isCircle  = false;
        if(afterundo){
            maxStep = step;
            afterundo = false;
        }
        save();
    }
    if (isTriangle === true) {
        drawTriangleCanvas1(e.clientX - paper.left + 8, e.clientY - paper.top, shapeX, shapeY);
        context2.clearRect(0, 0, canvas2.width, canvas2.height);
        isTriangle  = false;
        if(afterundo){
            maxStep = step;
            afterundo = false;
        }
        save();
    }
    if (isRectangle === true) {
        drawRectangleCanvas1(e.clientX - paper.left, e.clientY - paper.top, shapeX - 8, shapeY);
        context2.clearRect(0, 0, canvas2.width, canvas2.height);
        isRectangle  = false;
        if(afterundo){
            maxStep = step;
            afterundo = false;
        }
        save();
    }
    if (isLine === true) {
        drawLineCanvas1(e.clientX - paper.left, e.clientY - paper.top, shapeX - 8, shapeY);
        context2.clearRect(0, 0, canvas2.width, canvas2.height);
        isLine = false;
        if(afterundo){
            maxStep = step;
            afterundo = false;
        }
        save();
    }
    x = 0;
    y = 0;
    canvas2.style.zIndex = 0;
    canvas.style.zIndex = 1;
}

function mouseLeave(e) {
    console.log("mouseLeave");
    judgeLeave1 = 1;
    mouseUp(e);
}

function mouseLeave2(e) {
   console.log("mouseLeave2");
   mouseUp2(e);
}

function draw(x1, y1, x2, y2) {
    console.log("draw");
    context.beginPath();
    context.moveTo(x1-3, y1+25);
    context.lineTo(x2-3, y2+25);
    context.stroke();
    context.closePath();
}

function erase(x1, y1, x2, y2) {
    console.log("erase");
    context.beginPath();
    context.moveTo(x1+6, y1+15);
    context.lineTo(x2+6, y2+15);
    context.stroke();
    context.closePath();
}

function inputText(x1, y1){
    console.log("inputText");
    var input = document.createElement('input');
    input.id = 'newInput';
    input.type = 'text';
    input.style.left = (x1) + 'px';
    input.style.top = (y1) + 'px';
    input.style.position = 'absolute';
    input.style.zIndex = 2;
    div1.append(input);
    input.focus();
}

function finishInput(){
    console.log("finishInput");
    var newInput = document.getElementById('newInput')
        drawText(newInput.value, parseInt(newInput.style.left, 10), parseInt(newInput.style.top, 10));
        div1.removeChild(newInput);
        save();
}

function drawText(txt, x, y) {
    console.log("drawText");
    context.fillText(txt, x, y);
}

function drawCircleCanvas2(x2, y2, startX, startY){
    console.log("drawCircleCanvas2");
    var dx = Math.pow(x2-startX, 2);
    var dy = Math.pow(y2-startY, 2);
    var r = Math.sqrt(dx+dy)/2 ;
    context2.beginPath();
    context2.arc(startX + (x2-startX)/2, startY+(y2-startY)/2, r, 0, 2 * Math.PI);
    context2.stroke();
    context2.closePath();
}

function drawCircleCanvas1(x2, y2, startX, startY){
    console.log("drawCircleCanvas1");
    var dx = Math.pow(x2-startX, 2);
    var dy = Math.pow(y2-startY, 2);
    var r = Math.sqrt(dx+dy)/2 ;
    context.beginPath();
    context.arc(startX + (x2-startX)/2, startY+(y2-startY)/2, r, 0, 2 * Math.PI);
    context.stroke();
    context.closePath();
}

function drawTriangleCanvas2(x2, y2, startX, startY){
    console.log("drawTriangleCanvas2");
    context2.beginPath();
    context2.moveTo(startX, startY);
    context2.lineTo(2*startX-x2, y2);
    context2.lineTo(x2, y2);
    context2.lineTo(startX, startY);
    context2.stroke();
    context2.closePath();
}

function drawTriangleCanvas1(x2, y2, startX, startY){
    console.log("drawTriangleCanvas1");
    context.beginPath();
    context.moveTo(startX, startY);
    context.lineTo(2*startX-x2, y2);
    context.lineTo(x2, y2);
    context.lineTo(startX, startY);
    context.stroke();
    context.closePath();
}

function drawRectangleCanvas2(x2, y2, startX, startY){
    console.log("drawRectangleCanvas2");
    context2.beginPath();
    context2.strokeRect(startX, startY, x2-startX, y2-startY);
    context2.closePath();
}

function drawRectangleCanvas1(x2, y2, startX, startY){
    console.log("drawRectangleCanvas1");
    context.beginPath();
    context.strokeRect(startX, startY, x2-startX, y2-startY);
    context.closePath();
}

function drawLineCanvas2(x2, y2, startX, startY){
    console.log("drawLineCanvas2");
    context2.beginPath();
    context2.moveTo(startX-5, startY-5);
    context2.lineTo(x2-5, y2-5);
    context2.stroke();
    context.closePath();
}

function drawLineCanvas1(x2, y2, startX, startY){
    console.log("drawLineCanvas1");
    context.beginPath();
    context.moveTo(startX-5, startY-5);
    context.lineTo(x2-5, y2-5);
    context.stroke();
    context.closePath();
}

function initPencil() {
    console.log("initPencil");
    btnReset();
    var pencilBtn = document.getElementById('pencilBtn');
    pencilBtn.style.borderStyle = 'inset';
    document.body.style.cursor = "url(cur/pencil.cur), auto";
    pencil = true;
    setBrushSize();
    setBrushCap("round");
}

function initEraser() {
    console.log("initEraser");
    btnReset();
    var eraserBtn = document.getElementById('eraserBtn');
    eraserBtn.style.borderStyle = 'inset';
    document.body.style.cursor = "url(cur/eraser.cur), auto";
    eraser = true;
    context.lineWidth = 20;
    setBrushCap("square");
}

function initText() {
    console.log("initText");
    btnReset();
    var textBtn = document.getElementById('textBtn');
    textBtn.style.borderStyle = 'inset';
    document.body.style.cursor = "text";
    text = true;
}

function initCircle() {
    console.log("initCircle");
    btnReset();
    var circleBtn = document.getElementById('circleBtn');
    circleBtn.style.borderStyle = 'inset';
    document.body.style.cursor = "url(cur/circle.cur), auto";
    circle = true;
    setBrushCap("round");
}

function initTriangle() {
    console.log("initTriangle");
    btnReset();
    var triangleBtn = document.getElementById('triangleBtn');
    triangleBtn.style.borderStyle = 'inset';
    document.body.style.cursor = "url(cur/triangle.cur), auto";
    triangle = true;
    setBrushCap("round");
}

function initRectangle() {
    console.log("initRectangle");
    btnReset();
    var rectangleBtn = document.getElementById('rectangleBtn');
    rectangleBtn.style.borderStyle = 'inset';
    document.body.style.cursor = "url(cur/rectangle.cur), auto";
    rectangle = true;
    setBrushCap("round");
}

function initLine() {
    console.log("initLine");
    btnReset();
    var lineBtn = document.getElementById('lineBtn');
    lineBtn.style.borderStyle = 'inset';
    document.body.style.cursor = "crosshair";
    line = true;
}

function initDownload() {
    console.log("initDownload");
    var downloadBtn = document.getElementById('downloadBtn');
    downloadBtn.style.borderStyle = 'inset';
    context.globalCompositeOperation = 'source-over';
}

function Download() {
    console.log("Download");
    downloader.href = canvas.toDataURL();
    downloadBtn.style.borderStyle = 'outset';
}

function initUpload() {
    console.log("initUpload");
    var uploadBtn = document.getElementById('uploadBtn');
    uploadBtn.style.borderStyle = 'inset';
    context.globalCompositeOperation = 'source-over';
}

function loadFile(e){
    var newFile = e.files[0];
    var src = URL.createObjectURL(newFile);
    var img = new Image();
    img.src = src;
    img.onload = function(){
        context.drawImage(this, 0, 0, canvas.width, canvas.height);
        save();
    };
    uploadBtn.style.borderStyle = 'outset';
}

function initUndo() {
    console.log("initUndo");
    var undoBtn = document.getElementById('undoBtn');
    undoBtn.style.borderStyle = 'inset';
    context.globalCompositeOperation = 'source-over';
}

function Undo() {
    console.log("Undo");
    if (step > 0) {
        step--;
        console.log(step);
        let canvaspic = new Image();
        canvaspic.src = history[step];
        canvaspic.onload = function() {
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.drawImage(canvaspic, 0, 0);
        }
    }
    undoBtn.style.borderStyle = 'outset';
    afterundo = true;
}

function initRedo() {
    console.log("initRedo");
    var redoBtn = document.getElementById('redoBtn');
    redoBtn.style.borderStyle = 'inset';
    context.globalCompositeOperation = 'source-over';
}

function Redo() {
    console.log("Redo");
    if (step < maxStep) {
        step++;
        let canvaspic = new Image(); 
        canvaspic.src = history[step]; 
        canvaspic.onload = function() {
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.drawImage(canvaspic, 0, 0);
        }
    }
    redoBtn.style.borderStyle = 'outset';
}

function save() {
    console.log("save");
    step++;
    maxStep++;
    history[step] = canvas.toDataURL();
    console.log(step);
    console.log(maxStep);
}

function initReflesh() {
    console.log("initReflesh");
    var refleshBtn = document.getElementById('refleshBtn');
    refleshBtn.style.borderStyle = 'inset';
    context.globalCompositeOperation = 'source-over';
}

function Reflesh() {
    console.log("Reflesh");
    console.log('reflesh');
    console.log(step);
    console.log(maxStep);
    context.clearRect(0, 0, canvas.width, canvas.height);
    step = 0;
    maxStep = 0;
    refleshBtn.style.borderStyle = 'outset';
}

function btnReset() {
    console.log("btnReset");
    pencilBtn.style.borderStyle = 'outset'; 
    eraserBtn.style.borderStyle = 'outset';
    textBtn.style.borderStyle = 'outset';
    circleBtn.style.borderStyle = 'outset';
    triangleBtn.style.borderStyle = 'outset';
    rectangleBtn.style.borderStyle = 'outset';
    lineBtn.style.borderStyle = 'outset';
    downloadBtn.style.borderStyle = 'outset';
    uploadBtn.style.borderStyle = 'outset';
    undoBtn.style.borderStyle = 'outset';
    redoBtn.style.borderStyle = 'outset';
    refleshBtn.style.borderStyle = 'outset';

    document.body.style.cursor = "default";

    pencil = false;
    eraser = false;
    text = false;
    circle = false;
    triangle = false;
    rectangle = false;
    line = false;

    afterundo = false;
}

function downBrushSize() {
    console.log("downBrushSize");
    brushLeftBtn.style.borderStyle = 'inset';
    var p1Size = document.getElementById('p1Size');
    var p1SizeStr = p1Size.innerText;
    if(brushsize > 1){
        brushsize = parseInt(p1SizeStr, 10) - 1;
    }
    else{
        brushsize = 50;
    }
    p1Size.innerText = brushsize.toString();
}

function upBrushSize() {
    console.log("upBrushSize");
    brushRightBtn.style.borderStyle = 'inset';
    var p1Size = document.getElementById('p1Size');
    var p1SizeStr = p1Size.innerText;
    if(brushsize < 50){
        brushsize = parseInt(p1SizeStr, 10) + 1;
    }
    else{
        brushsize = 1;
    }
    p1Size.innerText = brushsize.toString();
}

function setBrushSize() {
    console.log("setBrushsize");
    context.lineWidth = brushsize;
    context2.lineWidth = brushsize;
    brushLeftBtn.style.borderStyle = 'outset';
    brushRightBtn.style.borderStyle = 'outset';
}

function setBrushColor() {
    console.log("setBrushcolor");
    var color = palette.value;
    context.strokeStyle = color;
    context.fillStyle = color;
    context2.strokeStyle = color;
}

function setBrushCap(type) {
    console.log("setBrushcap");
    context.lineCap = type;
}

function downFontSize() {
    console.log("downFontSize");
    fontSizeLeftBtn.style.borderStyle = 'inset';
    var p2Size = document.getElementById('p2Size');
    var p2SizeStr = p2Size.innerText;
    var num = parseInt(p2SizeStr, 10);
    if(num > 1){
        num--;
    }
    else{
        num = 50;
    }
    p2Size.innerText = num.toString();
    fontsize = num.toString() + 'px';
}

function upFontSize() {
    console.log("upFontSize");
    fontSizeRightBtn.style.borderStyle = 'inset';
    var p2Size = document.getElementById('p2Size');
    var p2SizeStr = p2Size.innerText;
    var num = parseInt(p2SizeStr, 10);
    if(num < 50){
        num++;
    }
    else{
        num = 1;
    }
    p2Size.innerText = num.toString();
    fontsize = num.toString() + 'px';
}

function downFontType() {
    console.log("downFontType");
    fontTypeLeftBtn.style.borderStyle = 'inset';
    var p3Type = document.getElementById('p3Type');
    if(typeindex > 0){
        typeindex--;
    }
    else{
        typeindex = 5;
    }
    p3Type.innerText = typename[typeindex];
}

function upFontType() {
    console.log("upFontType");
    fontTypeRightBtn.style.borderStyle = 'inset';
    var p3Type = document.getElementById('p3Type');
    if(typeindex < 5){
        typeindex++;
    }
    else{
        typeindex = 0;
    }
    p3Type.innerText = typename[typeindex];
}

function setFont() {
    context.font = fontsize + ' ' + typename[typeindex];
    fontSizeLeftBtn.style.borderStyle = 'outset';
    fontSizeRightBtn.style.borderStyle = 'outset';
    fontTypeLeftBtn.style.borderStyle = 'outset';
    fontTypeRightBtn.style.borderStyle = 'outset';
}

function initApp() {
    console.log("initApp");

    //upBtn event
    brushLeftBtn.addEventListener('mousedown', downBrushSize);
    brushLeftBtn.addEventListener('mouseup', setBrushSize);

    brushRightBtn.addEventListener('mousedown', upBrushSize);
    brushRightBtn.addEventListener('mouseup', setBrushSize);

    fontSizeLeftBtn.addEventListener('mousedown', downFontSize);
    fontSizeLeftBtn.addEventListener('mouseup', setFont);

    fontSizeRightBtn.addEventListener('mousedown', upFontSize);
    fontSizeRightBtn.addEventListener('mouseup', setFont);

    fontTypeLeftBtn.addEventListener('mousedown', downFontType);
    fontTypeLeftBtn.addEventListener('mouseup', setFont);

    fontTypeRightBtn.addEventListener('mousedown', upFontType);
    fontTypeRightBtn.addEventListener('mouseup', setFont);

    //downBtn event
    pencilBtn.addEventListener('click', initPencil);
    eraserBtn.addEventListener('click', initEraser);
    textBtn.addEventListener('click', initText);
    circleBtn.addEventListener('click', initCircle);
    triangleBtn.addEventListener('click', initTriangle);
    rectangleBtn.addEventListener('click', initRectangle);
    lineBtn.addEventListener('click', initLine);

    downloadBtn.addEventListener('mousedown', initDownload);
    downloadBtn.addEventListener('mouseup', Download);

    uploadBtn.addEventListener('mousedown', initUpload);
    uploadBtn.addEventListener('mouseup', e => {file.click()});

    undoBtn.addEventListener('mousedown', initUndo);
    undoBtn.addEventListener('mouseup', Undo);

    redoBtn.addEventListener('mousedown', initRedo);
    redoBtn.addEventListener('mouseup', Redo);

    refleshBtn.addEventListener('mousedown', initReflesh);
    refleshBtn.addEventListener('mouseup', Reflesh);

    //canvas event
    canvas.addEventListener('mousedown', e => {mouseDown(e)});
    canvas.addEventListener('mousemove', e => {mouseMove(e)});
    canvas.addEventListener('mouseup', e => {mouseUp(e)});
    canvas.addEventListener('mouseleave', e => {mouseLeave(e)});

    canvas2.addEventListener('mousemove', e => {mouseMove2(e)});
    canvas2.addEventListener('mouseup', e => {mouseUp2(e)});
    canvas2.addEventListener('mouseleave', e => {mouseLeave2(e)});
    context2.globalCompositeOperation = 'destination-atop';
    context2.lineCap = 'round';

  }

window.onload = function() {
    
    console.log("onload");
    //canvas
    window.canvas = document.getElementById('canvas');
    window.context = canvas.getContext('2d');
    window.paper = canvas.getBoundingClientRect();
    window.judgeLeave1 = 0;

    window.canvas2 = document.getElementById('canvas2');
    window.context2 = canvas2.getContext('2d');
    window.paper2 = canvas2.getBoundingClientRect();

    //upBtn
    window.brushLeftBtn = document.getElementById('brushLeftBtn');
    window.brushRightBtn = document.getElementById('brushRightBtn');
    window.fontSizeLeftBtn = document.getElementById('fontSizeLeftBtn');
    window.fontSizeRightBtn = document.getElementById('fontSizeRightBtn');
    window.fontTypeLeftBtn = document.getElementById('fontTypeLeftBtn');
    window.fontTypeRightBtn = document.getElementById('fontTypeRightBtn');

    window.palette = document.getElementById('palette');


    //downBtn    
    window.pencilBtn = document.getElementById('pencilBtn');
    window.eraserBtn = document.getElementById('eraserBtn');
    window.textBtn = document.getElementById('textBtn');
    window.circleBtn = document.getElementById('circleBtn');
    window.triangleBtn = document.getElementById('triangleBtn');
    window.rectangleBtn = document.getElementById('rectangleBtn');
    window.lineBtn = document.getElementById('lineBtn');
    window.downloadBtn = document.getElementById('downloadBtn');
    window.uploadBtn = document.getElementById('uploadBtn');
    window.undoBtn = document.getElementById('undoBtn');
    window.redoBtn = document.getElementById('redoBtn');
    window.refleshBtn = document.getElementById('refleshBtn');
    window.downloader = document.getElementById('downloader');

    window.file = document.getElementById('file');

    //leftDiv
    window.div1 =  document.getElementById('div1');

    //to store pic
    window.history = new Array();
    window.step = -1;
    window.maxStep = -1;

    //judge doing
    window.isDrawing = false;
    window.isErasing = false;
    window.isTexting = false;
    window.isCircle = false;
    window.isTriangle = false;
    window.isRectangle = false;
    window.isLine = false;
    window.x = 0;
    window.y = 0;
    window.shapeX = 0;
    window.shapeY = 0;

    //judge downBtn
    window.pencil = false;
    window.eraser = false;
    window.text = false;
    window.circle = false;
    window.triangle = false;
    window.rectangle = false;
    window.line = false;

    window.afterundo = false;

    //brush size color
    window.brushsize = 5;

    //font size color
    window.fontsize = '20px';
    window.typename = ['微軟正黑體', '標楷體', '新細明體', 'Arial', 'Verdana', 'Chiller']
    window.typeindex = 0;

    //初始值
    context.lineWidth = 5;
    context.strokeStyle = 'black';

    context2.lineWidth = 5;
    context2.strokeStyle = 'black';

    context.fillStyle = 'black';
    context.textBaseline = 'middle';
    context.textAlign = 'left';
    context.font = "20px 微軟正黑體";

    save();
    initApp();
}